# The protocol

The **Resource Sharing Protocol** (RSP) is a set of metadata intended to help free software groups and was **replaced in favour of** [ISO 1312](https://iso1312.fluxo.info).

* Share their available resources and also
* Find groups that can host services for them given their needs and requirements. 

The RSP splits each resource in a layer or set of service layers. You can think of service layers something very general, ranging from servers, databases, websites. If you need to, you can even consider stuff such as tables and bicycles as layers that provide some kind of service to a hosted group. ;)

The core protocol is composed by the following metadata:

* Version: RSP version.
* Date: when this metadata was generated.
* Next audit: time where metadata update will happen (if applicable).
* Layer type.
* Layer name (if applicable).
* Layer implementation (eg. if virtual zone: distro name, version, etc).
* Available disk space.
* Available memory.
* Available CPU.
* UPS capacity/characteristics.
* Average downtime.
* Timezone.
* Available system capabilities.
* Networking
  * Available bandwidth.
  * Real IP availability.
  * Proxy availability.
  * Firewall/routing availability.
  * VPN availability. 
* Who admins/updates the layer:
  * The hosting group?
  * The hosted group? 
* Security:
   * Security and privacy policy for the layer.
   * Crypto
      * Implementation.
      * Cipher.
      * Used procedure.
      * Shared password? Which sharing scheme? 
   * Boot procedure:
      * The host has automatic boot loading.
      * No boot loading in the host, manual boot. 
* Backups
    * Backup availability
      * No backups.
      * Local backups.
      * Remote backups in more than a single place but in the same building/jurisdiction/etc.
      * Remote backups in more than a single place and also in other countries. 
    * Backup security
      * Have unencrypted backups.
      * Have encrypted backups only.
      * Have encrypted backups only with a key that needs more than one sysadmin to unlock.
      * Have encrypted backups only with a key that the sysadmins do not have access to. 
    * Backup integrity
      * No integrity checking.
      * Signed backups only.
      * Push and signed backups.
      * Pull and signed backups only. 
    * Backup and users
      * Users are informed whether their private data are being backed up and in which ways.
      * Have backup data always available to private download by users and/or groups with admin aid.
      * Have backup data always available to private download by users and/or groups without admin aid.
      * Users and/or groups can choose to not have backups of their data. 
* Legal
  * Applicable law where layer is located.
  * Hosting group has legal team/support? 
  * Available copyright licenses allowed in the layer.
* Financial 

# Packages

Even if the metadata supports different configurations, it can be unsuitable to use all set for each application or layer specificiation, especially if you're dealing with multiple layers, one atop of another. Then, it is possible to have a more generic list of prerequisite information, along the lines of a debian package. these are the debian headings:

      Package:
      State:
      Automatically installed:
      Version:
      Priority:
      Section:
      Maintainer:
      Uncompressed Size:
      Depends:
      Suggests:
      Conflicts:
      Replaces:
      Provides:
      Description:

So possible descriptions for a layer could be something like:

      Package: [generic name/category]{server-vserver}
      State: [hardware, software, network, labor, physical item]{hardware}
      Reusable: [limited availability or recurring possibility?]{continuing}
      Version: [specific 'local' name]{project_name_1.0}
      Priority: [required/important/standard/optional/extra]{required}
      Section: [admin/backups/mirrors/doc/comms]{admin}
      Maintainer: [host/user]{host}
      Size: [amount or n/a]{10GB}
      Depends:
      Suggests: network-traffic (=project_name_*), encryption-policy (=project_name_*),
         reciprocation-policy (>=1.2)
      Conflicts: server-*,
      Replaces: vserver
      Provides: vserver, server-encryption
      Description: encrypted vserver
         This package provides a 10 GB vserver hosted on an
         individually encrypted single partition. Root administrative
         access is provided via ssh using key-based authentication, in
         line with standard encryption policy. Bandwidth can be added
         by using the network-bw_project_name_* packages.

Where

    key: [text in square brackets] = comment or possible options
         {text in squiggly brackets} = example

This way, RSP metadata is encapsulated in resource packages that can be announced, distributed or kept for internal use, alowing a given resource to have it's own dependency set and property inheritance.

# Rulesets

It is not strict builtin in the protocol but your group can also adopt internal rules regarding what goes at each layer. As an example, a group can adopt a data persistence rule such as data in a layer with a given level of security cannot be moved to a layer with a lower security level, except in cases it is temporarily or permanently declassified.

# Implementation

The RSP implementation does not come directly from the protocol. It can be implemented several ways, like keeping tables or wiki pages with detailed information about each layer a given group (or groups) have, can offer or needs. You could even consider to write a script to keep this data updated, although this can be very difficult. One of the best ways to keep the metadata updated is through periodical audits in each layer.

RSP is meant to be simple enough to be both human and machine readable. So, when considering an implementation, think about those two main types of readability.

# Applications

* Ask for resources.
* Resource announcement:
  * Offer a resource.
  * Ask others to copy a resource.
* Check if a resource exist (ping a website, etc).

# Metadata examples

A Linux-VServer instance could hold the following metadata values:

* Version: 01.
* Date: 20090409.
* Next audit: 20090820.
* Layer type: Linux VServer.
* Layer name: layer.example.org.
* Layer implementation: Debian 5.0.
* Available disk space: 20GB.
* Available memory: 500MB.
* ...

Another example could be a database:

* Version: 01.
* Date: 20090409.
* Next audit: 20090820.
* Layer type: SQL.
* Layer name: dbname@layer.example.org.
* Layer implementation: MySQL 5.1.
* ...

# Version

This is RSP Version: 0.1.

# Development

RSP development and maintenance ins't a hard task: it's just a matter of:

* Keep a backward-compatible list of metadata.
* Try to fit all needs into the set.
* KISS.

# Analysis

See [[analysis|analisys]].

# License

RSP is distributed under the [GNU Affero General Public License](http://www.gnu.org/licenses/agpl-3.0.html):

    Resource Sharing Protocol - RSP
    Copyright (C) 2009 RSP Development Team

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

----

This wiki is powered by [ikiwiki](http://ikiwiki.info).
