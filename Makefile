#
#  Ikiwiki Makefile by Silvio Rhatto (rhatto at riseup.net).
#
#  This Makefile is free software; you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by the Free
#  Software Foundation; either version 3 of the License, or any later version.
#
#  This Makefile is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
#  Place - Suite 330, Boston, MA 02111-1307, USA
#

web:
	@ikiwiki --setup ikiwiki.setup
	#@chmod +x bin/*

web_deploy:
	#git push web
	#git annex sync
	@rsync -avz --delete www/ rsp:/var/sites/rsp/www/

post_update:
	git config receive.denyCurrentBranch ignore
	cd .git/hooks && ln -sf ../../bin/post-update

post_receive:
	git config receive.denyCurrentBranch ignore
	cd .git/hooks && ln -sf ../../bin/post-receive

whoami:
	git config user.name "User"
	git config user.email user@example.org

publish: web web_deploy
